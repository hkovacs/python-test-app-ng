import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  public login(userInfo: User) {
    console.log('userInfo', userInfo);

    this.http.post<any>('http://127.0.0.1:8000/login', userInfo).subscribe(res => {
      console.log('res', res.user_id);
      localStorage.setItem('ACCESS_TOKEN', "access_token");
      if (res.user_id) {
        this.router.navigateByUrl('/admin');
      }
    });

}

  public isLoggedIn() {
  return localStorage.getItem('ACCESS_TOKEN') !== null;

}

  public logout() {
  localStorage.removeItem('ACCESS_TOKEN');
}
}
